package cz.pkupka.coreproject.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {

    @RequestMapping({"/hellouser"})
    public String helloUser(){
        return "Hello User";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping({"/helloadmin"})
    public String helloAdmin(){
        return "Hello Admin";
    }

}
