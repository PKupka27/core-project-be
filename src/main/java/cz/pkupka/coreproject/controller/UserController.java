package cz.pkupka.coreproject.controller;

import cz.pkupka.coreproject.model.DAOUser;
import cz.pkupka.coreproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public List<DAOUser> getUsers() {
        return userRepository.findAll();
    }
}
