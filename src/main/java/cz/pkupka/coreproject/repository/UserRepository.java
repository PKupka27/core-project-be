package cz.pkupka.coreproject.repository;

import cz.pkupka.coreproject.model.DAOUser;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<DAOUser, String> {

    DAOUser findByUsername(String username);
}
